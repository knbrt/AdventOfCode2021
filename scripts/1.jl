lineText = []
open("data/01/input.txt", "r") do f
    global lineText
    lineText = [parse(Int, x) for x in readlines(f)]
end

res = []
for i in 2:size(lineText)[1]
    lineText[i] > lineText[i-1] && push!(res, i)
end



res = []
for i in 4:size(lineText)[1]
    sum(lineText[i-2:i]) > sum(lineText[i-3:i-1]) && push!(res, i)
end

