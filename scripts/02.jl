lines = readlines("data/02/input.txt")

frwd = filter( x -> startswith(x, "forward"), lines)
down = filter( x -> startswith(x, "down"), lines)
up = filter( x -> startswith(x, "up"), lines)

# part 1
Σfrwd = sum(map(x -> parse(Int, x[end]), frwd))
Σdown = sum(map(x -> parse(Int, x[end]), down))
Σup = sum(map(x -> parse(Int, x[end]), up))

Σdepth = Σdown - Σup

ans = Σfrwd * Σdepth


# part 2
mutable struct submarine
    hPos::Int
    depth::Int
    aim::Int
    function submarine(hPos, depth, aim)
        new(hPos, depth, aim)
    end
end

function step!(sub::submarine, key::String, val::Int)
    key == "down" && (sub.aim += val)
    key == "up" && (sub.aim += (-1) * val)
    key == "forward" && begin
        sub.hPos += val
        sub.depth += sub.aim * val
    end
end

function step!(sub::submarine, input::String)
    key, val = split(input, " ")
    step!(sub, String(key), parse(Int, val))
end

s = submarine(0,0,0)
for l in lines
    step!(s, l)
end

s.hPos * s.depth

