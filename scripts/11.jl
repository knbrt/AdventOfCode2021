
m = Matrix{Int}(undef, 10,10)

file = []
open("data/11/input.txt", "r") do f
    push!(file, [parse.(Int, split("$(x)", "")) for x ∈ readlines(f)]...)
end

for i ∈ 1:10, j ∈ 1:10
    m[i,j] = file[i][j]
end

flash = 0

function step!(m::Matrix{Int}, flash::Int)
    m = m .+1
    flash = doFlashes(m, flash)
    return m, flash
end

function doFlashes(m::Matrix{Int}, flash::Int)
    k = m .> 9
    for i ∈ eachindex(k)
        m[i] 
    end
    return m, flash
end


for _ ∈ 1:100
    m, flash = step!(m, flash)
end



