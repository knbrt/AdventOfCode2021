lines = readlines("data/03.txt")

# part 1
unique(length.(lines))

γ = []
ϵ = []
for i ∈ 1:unique(length.(lines))[1]
    t = []
    for l ∈ lines
        push!(t, l[i])
    end
    t₁= [(x => count(==(x), t)) for x ∈ unique(t)]
    tγ = sort(t₁, by= x -> -last(x))[1]
    tϵ = sort(t₁, by= x -> last(x))[1]
    push!(γ, first(tγ))
    push!(ϵ, first(tϵ))
end

γ_num = parse(Int, join(γ), base=2)
ϵ_num = parse(Int, join(ϵ), base=2)

res = γ_num * ϵ_num

